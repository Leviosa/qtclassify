#! /usr/bin/env python

### Creates multiple plots of different positions in spectrum where lines
### are assumed, in order to make it easier to classify the object

################################################################################

import sys, os
import argparse
import numpy as np
import functools

from astropy import wcs
from astropy.io import fits
from astropy.coordinates import Angle
from astropy import units as u

import scipy.ndimage

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
from pyqtgraph.Qt import QtWidgets

# This will ignore ALL warnings... Not ideal, though.
import warnings
warnings.filterwarnings("ignore")

# get lines from own module
from custom_options_py3 import possible_lines_names, dict_other_lines, check_opts
from custom_lines_py3 import lbda, possible_identifications, poss_ident_short, check_dicts

# immediately check if input is ok
check_dicts(lbda, possible_identifications, poss_ident_short)
check_opts(possible_lines_names, dict_other_lines, lbda)

################################################################################
######################### FILES AND DATACUBES ... ##############################
################################################################################

# location of the script
abspath = os.path.dirname(__file__)
if abspath == '':
    abspath = './'  # -> python line_classification_GUI_pyqtgraph.py
else:
    abspath = abspath + '/'

### Set default values for the cubes, the catalog and the output
directory = '/home/hector/Documents/PhD/Classify/fields/'
field_dir = 'candels-cdfs-01/'
def_cube = (directory + field_dir +
            'DATACUBE_MFS_candels-cdfs-01_v2.0.eff.wor.fits')
def_cube_sn = (directory + field_dir + 's2n_candels-cdfs-01_v2.0.fits')
# Catalog that will be used has to have ID, X_SN, Y_SN, Z_SN and LAMBDA_SN
def_catalog = directory + field_dir + 'catalog_candels-cdfs-01_fcut_fluxes_cut.fits'  # default
# New catalog with your identifications
def_new_catalog = directory + field_dir + 'catalog_candels-cdfs-01_fcut_fluxes_classified.fits'
# HST image
def_HST = directory + field_dir + 'acs_814w_candels-cdfs-01_cut.fits'

### Read the input and output files from the command line
command = os.path.basename(sys.argv[0])
for entity in sys.argv[1:]:
    command = command + ' ' + entity

parser = argparse.ArgumentParser(description="""
line_classification_GUI_pyqtgraph.py - classify lines with a GUI!
""", formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument("-id", "--inputdata", required=False, type=str,
                    default=def_cube, help="""
Input flux cube FITS file (e.g. median filter subtracted fluxcube).
""")
parser.add_argument("-isn", "--inputsn", required=False, type=str,
                    default=def_cube_sn, help="""
Input S/N cube FITS file from LSDCat.
""")
parser.add_argument("-c", "--catalog", required=False, type=str,
                    default=def_catalog, help="""
Input catalog FITS file from LSDCat.
""")
parser.add_argument("-o", "--output", required=False, type=str,
                    default=def_new_catalog, help="""
Output catalog FITS file.
""")
parser.add_argument("-F", "--fluxhdu", default=1, type=int, help="""
HDU Number (0-indexed) of flux in input flux cube FITS file (default: 1).
""")
parser.add_argument("-N", "--noisehdu", default=2, type=int, help="""
HDU Number (0-indexed) of variance in input flux cube FITS file (default: 2).
If set to -1, the noise will not be used (saves memory).
""")
parser.add_argument("-c_ID", "--column_ID", required=False, default='ID',
                    type=str, help="""
Column name in input FITS catalog for ID, default=ID.
""")
parser.add_argument("-c_X", "--column_X", required=False, default='X_SN',
                    type=str, help="""
Column name in input FITS catalog for x position (in pixel), default=X_SN.
""")
parser.add_argument("-c_Y", "--column_Y", required=False, default='Y_SN',
                    type=str, help="""
Column name in input FITS catalog for y position (in pixel), default=Y_SN.
""")
parser.add_argument("-c_Z", "--column_Z", required=False, default='Z_SN',
                    type=str, help="""
Column name in input FITS catalog for z position (in pixel), default=Z_SN.
""")
parser.add_argument("-c_RA", "--column_RA", required=False, default='RA_SN',
                    type=str, help="""
Column name in input FITS catalog for RA (in degrees), default=RA_SN.
""")
parser.add_argument("-c_DEC", "--column_DEC", required=False, default='DEC_SN',
                    type=str, help="""
Column name in input FITS catalog for DEC (in degrees), default=DEC_SN.
""")
parser.add_argument("-c_LAM", "--column_LAM", required=False, default='LAMBDA_SN',
                    type=str, help="""
Column name in input FITS catalog for lambda (wavelength in Angstrom),
default=LAMBDA_SN.
""")
parser.add_argument("-c_SN", "--column_SN", required=False, default='DETSN_MAX',
                    type=str, help="""
Column name in input FITS catalog for maximum signal to noise for the found line
(DETSN_MAX in LSDCat),  default=DETSN_MAX.
""")
parser.add_argument("-hst", "--hstimage", required=False, default=def_HST,
                    type=str, help="""
                    HST image for comparison.
                    """)
parser.add_argument("-v", "--vacuum", required=False, type=str, default=1,
                    help="""
Convert the wavelength of found lines from air to vacuum for determining the
redshift. Set to 1 if you want the conversion, set to 0 if not.
""")
parser.add_argument("-e", "--eff_noise", required=False, default=False, type=str,
                    help="""
Instead of using a noise cube (or the noise HDU of a datacube) it is also
possible to use the 1D effective noise, which is scaled according to the
aperture size.
""")
parser.add_argument("-EN", "--effnoisehdu", default=0, type=int, help="""
HDU Number (0-indexed) of 1D effective noise FITS file (default: 0).
""")
parser.add_argument("-PN", "--pipenoisehdu", default=1, type=int, help="""
HDU Number (0-indexed) of 1D  pipepline noise FITS file (default: -1). If set
to -1, the noise will not be used.
""")
parser.add_argument("-rcn", "--replaceCubeNaNs", required=False, default='True',
                    type=str, help="""
Set this keyword to "False" to ignore the memory and time consuming replacement
of NaNs with 0s when loading data cubes
""")

parser.add_argument("-d", "--design", required=False, default='old', type=str,
                    help="""
Set this keyword to 'new' for the new design using viridis and magma, to 'old'
for cubehelix.
""")

args = parser.parse_args()

cube = args.inputdata
cube_sn = args.inputsn
catalog = [args.catalog]
new_catalog = [args.output]
hst_image = args.hstimage
fluxhdu = args.fluxhdu
noisehdu = args.noisehdu
effnoisehdu = args.effnoisehdu
pipenoisehdu = args.pipenoisehdu
col_id = args.column_ID
col_x = args.column_X
col_y = args.column_Y
col_z = args.column_Z
col_ra = args.column_RA
col_dec = args.column_DEC
col_lam = args.column_LAM
col_sn = args.column_SN
air_vac = args.vacuum
eff_noise_file = args.eff_noise
replaceNaNs = args.replaceCubeNaNs
des = args.design

if des == 'new':
    # make viridis and others available
    from colormaps import magma_simple as magma
    from colormaps import inferno_simple as inferno
    from colormaps import plasma_simple as plasma
    from colormaps import viridis_simple as viridis

try:
    here_datacube = cube.find('DATACUBE')
    field_name = 'Field: '+cube[here_datacube+9:-5]
except:
    field_name = 'QtClassify'

if replaceNaNs == 'True':
    replaceNaNs = True
elif replaceNaNs == 'False':
    replaceNaNs = False
else:
    print(' Invalid value of "replaceCubeNaNs" argument; setting to default of "True" ')
    replaceNaNs = True

################################################################################
################################################################################

### specify parameters and layout options

ap = [1.5]  # aperture for spectra extraction in acrsec
width = 50  # half the width of the wavelength window that is shown
width_window = [8.]  # width of the thumbnail cut-out windows in arcsec

# the default would be a black background and white plot for pyqtgraph
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')

# values for the cubehelix colormap
ch_g = 1.0
ch_s = 1.7
ch_r = -1.5
ch_h = 0.7

# default values for smooth and shift
smooth_more = [0]
zoom = [10]
yrange = [2000]

# default cuts for the data, s/n and hst images
cut_1 = [-10]
cut_2 = [20]
cut_sn_1 = [1]
cut_sn_2 = [5]

# colors

c_otherlines = 'm'
c_otherfoundlines = 'g'
c_annotate = 'b'
c_spec = 'b'
c_mainline = 'g'
c_hstline = 'r'
c_ch = 'k'  # crosshair
c_p1 = (50, 100, 100)  # color of the zero line
c_noise = 'r'  # (200, 10, 200)
c_region1 = (70, 20, 100, 80)  # color of shaded area to show part of spectrum
c_region2 = (0, 0, 255, 80)  # color of shaded area to show part of spectrum

if des == 'new':
    from get_colors_py3_new import magma_more, viridis_green, viridis_more  # Changed July 2022

    # c_otherlines = np.asarray(magma_more[9])*255
    c_otherlines = np.asarray(viridis_more[9])*255
    c_otherfoundlines = np.asarray(viridis_green[4])*255
    c_annotate = np.asarray(viridis_green[2])*255
    c_spec = np.asarray(viridis_green[2])*255
    c_mainline = np.asarray(viridis_green[3])*255
    c_hstline = np.asarray(magma_more[6])*255
    c_p1 = np.asarray(viridis_green[0])*255  # zero line
    c_ch = np.asarray(viridis_green[1])*255  # zero line # crosshair
    # c_noise = np.asarray(magma_more[6])*255
    c_noise = np.asarray(viridis_more[6])*255
    c_region1 = tuple(np.asarray(viridis_green[4])*255)+(80, )
    c_region2 = tuple(np.asarray(viridis_green[3])*255)+(80, )

################################################################################

### Important functions


def read_hdu(infile, hdunum, nans_to_value=False, nanvalue=0, memmap=True):
    """
    image_data, header = read_hdu(infile, hdunum, nans_to_value=False, nanvalue=0)
    Read in a HDU <hdunum> from FITS-File <infile>, returns the
    data <image_data> and the header <header>.
    Additionally with nans_to_value = True (default: False) one can set
    all NaNs to a specific <nanvalue> (default: 0)

    """
    hdu = fits.open(infile, memmap=memmap)
    header = hdu[hdunum].header
    image_data = hdu[hdunum].data
    hdu.close()
    if nans_to_value == True:
        # change NaNs to nanvalue
        select = np.isnan(image_data)
        image_data[select] = nanvalue
    return image_data, header


def vminvmax(image, scale=0.99, nans=0):
    """
    vmin, vmax = vminvmax(data, scale=0.99, nans=0)

    In:
    ---
    data - 2D array (intensity image)
    scale (=0.99) - scale factor for colorbar
    nans (=0) value with wich NaNs in the array are replaced

    Out:
    ---
    vmin, vmax -- values to be used in matplotlib.imshow as vmin & vmax
    """
    assert scale <= 1 and scale > 0
    data = image.copy()

    data[np.isnan(data)] = nans
    if len(data.shape) > 1:
        data = data.flatten()
    data = np.sort(data)

    length = len(data)

    vmax_index = int(np.floor(scale*length))
    vmin_index = int(np.ceil((1-scale)*length))

    vmin = data[vmin_index]
    vmax = data[vmax_index]

    return vmin, vmax


def spec_in_ap(xcen, ycen, data, mode, ap, pix_size):
    """
    create mask in shape of a circle
    give summed spectra in the aperture
    xpos and ypos in pixel: coordinates of an object
    ap in arcsec
    ##### It follows: Christians Code
    """

    # cube dimensions
    xmax, ymax, zmax = data.shape[2], data.shape[1], data.shape[0]
    spax_size = pix_size

    # (spatial) pixel grid
    ygrid, xgrid = np.indices((ymax, xmax), float)

    # und so extrahiert man dann ein spektrum
    ext_radius = ap  # Apperturradius in Bogensekunden
    dy = ygrid - ycen
    dx = xgrid - xcen
    radius = np.sqrt(dy**2 + dx**2)

    select = radius < float(ext_radius) / float(spax_size)
    data = data[:, select]
    data[np.isnan(data)] = 0.0
    if mode == 'sum':
        spec_signal = data.sum(axis=1)
    elif mode == 'mean':
        spec_signal = data.mean(axis=1)
    elif mode == 'noise':
        spec_signal = np.sqrt(data.sum(axis=1))
        # spec_signal /= np.sqrt(float(np.sum(select)))
    return spec_signal


def cubehelix(gamma=1.0, s=0.5, r=-1.5, h=1.0):
    """
    creates a lookup table for the cubehelix colormap
    """
    def get_color_function(p0, p1):
        def color(x):
            xg = x ** gamma
            a = h * xg * (1 - xg) / 2
            phi = 2 * np.pi * (s / 3 + r * x)
            return xg + a * (p0 * np.cos(phi) + p1 * np.sin(phi))
        return color

    array = np.empty((256, 3))
    abytes = np.arange(0, 1, 1/256.)
    array[:, 0] = get_color_function(-0.14861, 1.78277)(abytes) * 255
    array[:, 1] = get_color_function(-0.29227, -0.90649)(abytes) * 255
    array[:, 2] = get_color_function(1.97294, 0.0)(abytes) * 255
    return array


def radec_to_pix(ra_coords, dec_coords, header, origin=0):
    """
    Converts RA & DEC world coordinates to pixel coordinates.

    In:
    ---
    ra_coords ... 1D array of RA in degrees (float)
    dec_coords ... 1D array of corresponding DEC in degrees (float)
    header ... an astropy.io.fits header object
    origin ... output coordinates 0-indexed if 0, or 1-indexed if 1
               (default: 0)

    Out:
    ---
    x_coords, y_coords ... 2-tuple of 1D arrays with pixel coordinates
    """

    wcs_obj = wcs.WCS(header, relax=False)  # no HST only spec
                                            # allowed, only WCS
                                            # standard
    coords = wcs_obj.wcs_world2pix(ra_coords, dec_coords, origin)
    x_coords = coords[0]
    y_coords = coords[1]
    return x_coords, y_coords


def pix_scale(header):
    """
    Prints out averaged pixel scale in arcseconds at reference pixel.
    Ignores dissortions, but this OK considering our small field of view.

    In:
    ---
    header ... an astropy.io.fits header object

    Out:
    ---
    pix_scale ... average linear extent of a pixel in arc-seconds
    """

    wcs_obj = wcs.WCS(header, relax=False)
    scale_deg_xy = wcs.utils.proj_plane_pixel_scales(wcs_obj)
    scale_deg = np.sum(scale_deg_xy[:2]) / 2.

    scale_deg = Angle(scale_deg, unit=u.deg)
    scale_asec = scale_deg.arcsec

    return scale_asec


def air_to_vac(air_wavel):
    # taken from (eq. 65):
    # http://www.aanda.org/articles/aa/full/2006/05/aa3818-05/aa3818-05.html
    # Greisen et al. 2005

    # assuming air_wavel is in Angstrom
    air_wavel = air_wavel/(10**4)  # equation is for micrometer

    n = 1+10**(-6)*(267.6155+1.62887/(air_wavel**2)+0.0136/(air_wavel**4))
    vac_wavel = (n*air_wavel)*(10**4)  # convert back to Angstrom
    if air_vac == 1:
        return vac_wavel
    else:
        return air_wavel*(10**4)


def write_output(catalog, lines_ident, lines_comment, lines_redshifts,
                 lines_quality, lines_confidence, poss_ident_short, lines_assoc,
                 new_catalog):
    # write identifications in fits file

    short_names = []
    for a in lines_ident:
        try:
            short_names.append(poss_ident_short[a])
        except KeyError:
            short_names.append('Rev')

    orig_table = fits.oorig_table = fits.open(catalog)[1].data
    orig_cols = orig_table.columns

    max_len = 0
    for comment in lines_comment:
        if len(comment) > max_len:
            max_len = len(comment)
    max_len += 1

    c1 = fits.Column(name='Identification', format='20A', array=lines_ident)
    c2 = fits.Column(name='Comment', format=str(max_len)+"A", array=lines_comment)
    c3 = fits.Column(name='Redshift', format="D", array=lines_redshifts)
    c4 = fits.Column(name='Quality', format="10A", array=lines_quality)
    c5 = fits.Column(name='Confidence', format="10A", array=lines_confidence)
    c6 = fits.Column(name='Short', format="10A", array=short_names)
    c7 = fits.Column(name='Association', format="10A", array=lines_assoc)

    column_list = []
    for a in range(len(orig_cols)):
        if orig_cols[a].name not in list(info_line.keys()):
            column_list.append(orig_cols[a])

    column_list.append(c1)
    column_list.append(c6)
    column_list.append(c2)
    column_list.append(c3)
    column_list.append(c4)
    column_list.append(c5)
    column_list.append(c7)

    table_hdu = fits.BinTableHDU.from_columns(column_list)
    table_hdu.writeto(new_catalog[0], overwrite=True)


class Check_filename_output(QtWidgets.QWidget):

    def __init__(self):
        super(Check_filename_output, self).__init__()

        self.new_catalog = new_catalog[0]
        self.initUI(new_catalog)

    def initUI(self, new_catalog):

        title = QtWidgets.QLabel('The output catalog "'+self.new_catalog+'" already exists. \nDo you want to use it as input instead and continue from your last classification? \nIf not, rename output catalog and start from scratch:', self)

        self.btn = QtWidgets.QPushButton('Rename', self)
        self.btn.clicked.connect(self.rename_clicked)

        self.btn2 = QtWidgets.QPushButton('Continue', self)
        self.btn2.clicked.connect(self.continue_clicked)

        self.le = QtWidgets.QLineEdit(self)
        self.le.setText('New name')

        grid = QtWidgets.QGridLayout()
        grid.setSpacing(10)
        grid.addWidget(title, 0, 0, 1, 2)
        grid.addWidget(self.le, 1, 0, 1, 2)
        grid.addWidget(self.btn, 2, 0)
        grid.addWidget(self.btn2, 2, 1)

        self.setLayout(grid)
        self.setGeometry(100, 100, 400, 200)
        self.setWindowTitle('Overwrite?')
        self.show()

    def rename_clicked(self):
        new_catalog[0] = self.le.text()
        self.close()

    def continue_clicked(self):
        catalog[0] = self.new_catalog
        self.close()


################################################################################


def main():

    app = QtWidgets.QApplication(sys.argv)
    main = Check_filename_output()
    main.show()
    app.exec()


if os.path.isfile(new_catalog[0]):
    if __name__ == '__main__':
        main()
catalog = catalog[0]


### Read catalog
try:
    print('Reading in the catalog... ' + catalog)
    input_file = catalog
    hdu = fits.open(input_file)
    header_cat = hdu[1].header
    data = hdu[1].data
    cols = hdu[1].columns
    cols_names = cols.names
    hdu.close()
except:
    print('Could not find catalog!')
    exit()

if col_id not in cols_names or col_sn not in cols_names:
    sys.exit('\n ERROR: You need a column containing IDs and a column containing S/N in your input catalogue. \n')
else:
    ID_first = data[col_id]
    detsn_max = data[col_sn]

# If ID is not an integer, there will be errors
ID = []
for all_ids in range(len(ID_first)):
    ID.append(int(float(ID_first[all_ids])))
ID = np.asarray(ID)

try:
    # reading the xyz coordinates if available
    if col_x in cols_names and col_y in cols_names and col_z in cols_names:
        x_sn = data[col_x]
        y_sn = data[col_y]
        z_sn = data[col_z]
        xyz = True
    else:
        xyz = False
except:
    xyz = False

try:
    # reading the RA, DEC and lambda coordinates if available
    if col_ra in cols_names and col_dec in cols_names and col_lam in cols_names:
        RA_sn = data[col_ra]
        DEC_sn = data[col_dec]
        lam_sn = data[col_lam]
        RaDecLam = True
    else:
        RaDecLam = False
except:
    RaDecLam = False

# if neither xyz nor RA, DEC and lambda are given, exit
if xyz == False and RaDecLam == False:
    sys.exit('\n ERROR: You need to provide either x, y and z positions or RA, DEC and lambda positions (or both). \n')

try:
    lines_ident = data['Identification']
    lines_comment = data['Comment']
    lines_quality = data['Quality']
    lines_redshifts = data['Redshift']
except KeyError:
    ### Prepare the identification by making empty arrays
    lines_ident = ["-" for x in range(len(ID))]
    lines_comment = ["-" for x in range(len(ID))]
    lines_quality = ["-" for x in range(len(ID))]
    lines_redshifts = np.zeros(len(ID))

# This makes sure that if the comments were restricted to some length before,
# this restriction is now lifted
lines_comment_new = ['-' for a in lines_comment]
lines_comment_new[:] = lines_comment
lines_comment = lines_comment_new

try:
    lines_confidence = data['Confidence']
except:
    lines_qual_a = [i for i in np.arange(0, len(lines_quality), 1)
                    if lines_quality[i] == 'a']
    lines_confidence = np.asarray(["0" for x in range(len(ID))])
    # set all quality a lines to confidence 3
    if len(lines_qual_a) > 0:
        lines_confidence[np.asarray(lines_qual_a)] = 3
try:
    lines_short = data['Short']
except:
    lines_short = ["-" for x in range(len(ID))]
try:
    lines_assoc = data['Association']
except KeyError:
    lines_assoc = ["-" for x in range(len(ID))]

# This makes sure that if the association was restricted to some length before,
# this restriction is now lifted
lines_assoc_new = ['-' for a in lines_assoc]
lines_assoc_new[:] = lines_assoc
lines_assoc = lines_assoc_new

start_again = [str(i) for i in range(len(lines_ident)) if lines_ident[i] == '-'
               or lines_ident[i] == 'Revisit']
if len(start_again) == 0:
    start_again_here = ['None']
    start_again_here_ID = ['None']
    num_uncl = [0]
else:
    start_again_here = [start_again[0]]
    start_again_here_ID = [str(ID_first[int(start_again_here[0])])]

### Read cube
print('Reading in the cube... '+cube+'['+str(fluxhdu)+']')
cube_data,  header = read_hdu(cube, fluxhdu, nans_to_value=replaceNaNs)
start_wavelength = header['CRVAL3']
try:
    wavelength_pp = header['CD3_3']
except:
    wavelength_pp = header['CDELT3']
z_length = header['NAXIS3']
pix_size = pix_scale(header)
wavelength = ((np.arange(0, z_length)*wavelength_pp)+start_wavelength)
wavelength = np.round(wavelength, 0)
stop_wavelength = wavelength[-1]

### convert RA, DEC and lambda from the catalogue to x, y and z using the
### header from the data cube or the other way around
wcs_obj = wcs.WCS(header, relax=False)

# test if the x, y coordinates in the table make sense, otherwise don't use them
if RaDecLam and xyz:
    coords = wcs_obj.wcs_world2pix(RA_sn, DEC_sn, lam_sn*10**(-10), 0)
    test_x_sn = coords[0]
    test_coords = int(test_x_sn[0]) == int(x_sn[0])
    if not test_coords:
        print("\n Attention! Your x and y positions in your catalogue do not match the RA and DEC coordinates! \n")
else:
    test_coords = True

if xyz == False or not test_coords:
    print("No x, y and z coordinates in catalgue. Computing x, y and z coordinates from given RA, DEC and lambda...")
    coords = wcs_obj.wcs_world2pix(RA_sn, DEC_sn, lam_sn*10**(-10), 0)
    x_sn = coords[0]
    y_sn = coords[1]
    z_sn = coords[2]
if RaDecLam == False:
    print("No Ra, Dec and lambda coordinates in catalogue. Computing Ra, Dec and lambda coordinates from given x, y and z...")
    coords = wcs_obj.wcs_pix2world(x_sn, y_sn, z_sn, 0)
    RA_sn = coords[0]
    DEC_sn = coords[1]
    lam_sn = coords[2]
    if lam_sn[0] < 1:  # lambda was given in m instead of Angstrom
        lam_sn = lam_sn*10**10

# sort the objects into individual lists and get highest sn object
lsd_cat_data = dict()  # contains index of lines for each object (ID)
for i in ID:
    lsd_cat_data[i] = (np.where(ID == i)[0])

total_num_obj = len(lsd_cat_data)
high_objs = {}  # contains index of lines with highest SN in catalog
for ids in lsd_cat_data:
    objs = lsd_cat_data[ids]
    detsn_max_objs = detsn_max[lsd_cat_data[ids]]
    here = np.where(np.max(detsn_max_objs) == detsn_max_objs)[0][0]
    high_objs[ids] = lsd_cat_data[ids][here]

# get number of unclassfied objs
unclassified = np.asarray([int(i) for i in start_again if
                                    int(i) in high_objs.values()])
num_uncl = [len(unclassified)]


### Read noise cube
if noisehdu == -1:
    print('No noise cube...')
    noise_cube = False
else:
    print('Reading in the noise cube... ' + cube + '[' + str(noisehdu) + ']')
    cube_noise_data, cube_noise_header = read_hdu(cube, noisehdu,
                                                  nans_to_value=replaceNaNs)
    noise_cube = True


if eff_noise_file != False:
    print("Reading in the effective noise... " + eff_noise_file)
    eff_noise_data = fits.open(eff_noise_file)
    eff_noise_spec = eff_noise_data[effnoisehdu].data  # eff. noise
    eff_noise_unit = eff_noise_data[effnoisehdu].header['BUNIT']

    # check if pipeline noise is included
    if pipenoisehdu != -1:
        pipeline_noise = eff_noise_data[pipenoisehdu].data

        # pipe noise and eff noise from same fits file should have different HDU
        if effnoisehdu == pipenoisehdu:
            print('\n Attention! Effective noise HDU and piepline noise HDU are the same.\n')

        # in places where eff.noise is below the pipeline propagated noise,  use
        # the latter

        for a in range(len(eff_noise_spec)):
            if eff_noise_spec[a] < pipeline_noise[a]:
                eff_noise_spec[a] = pipeline_noise[a]

    eff_noise_data.close()

if noise_cube == True and eff_noise_file != False:
    print("You specified a noise cube and the effective noise. The effective noise will be used.")


print('Reading in S/N cube... ' + cube_sn)
cube_data_sn, cube_data_sn_header = read_hdu(cube_sn, 1, nans_to_value=replaceNaNs)

### Reading in the hst image
try:
    print('Reading in HST image ... ' + hst_image)
    hst_data, hst_header = read_hdu(hst_image, 0, nans_to_value=True)
    hst_ps = pix_scale(hst_header)
    hst_image = True
    show_hst_now = [True]
    hst_level_min, hst_level_max = vminvmax(hst_data)
    cut_hst1 = [hst_level_min]
    cut_hst2 = [hst_level_max]

except:  # if there is no HST image, don't display one
    print('No HST image...')
    hst_image = False
    show_hst_now = [False]
    cut_hst1 = [0]
    cut_hst2 = [1]


### Reading in the rainbow3 MIDAS colormap
rainbow3_file = abspath + 'rainbow3.lasc'
rainbow3_file_open = open(rainbow3_file, 'r')
rainbow3_file_data = np.genfromtxt(rainbow3_file_open)*255
rainbow3_data = [row[0] for row in rainbow3_file_data]
rainbow3_file_open.close()

print('finished reading data...')

possible_lines = sorted(list({k: lbda[k] for k
                              in possible_lines_names}.values()))

all_lines = np.asarray(list(lbda.values()))
all_lines_names = np.asarray(list(lbda.keys()))

################################################################################
################################################################################

info_line = {'Identification': lines_ident,
             'Redshift': lines_redshifts,
             'Quality': lines_quality,
             'Confidence': lines_confidence,
             'Comment': lines_comment,
             'Short': lines_short,
             'Association': lines_assoc}

j = 0   # counts where you are (index of all entries in catalog)

first_ask = [False]  # ask if you want to overwrite the first time
custom_button_label = ['?']
custom_combo_index = [0]


class main_Window(QtWidgets.QMainWindow):

    signal_j = QtCore.pyqtSignal(int)

    def __init__(self):
        super(main_Window, self).__init__()

        self.initUI()

    def initUI(self):

        self.j = j
        self.one_or_all = 'all'  # default is to look at all lines

        if self.j == 0:

            self.main_GUI = main_GUI(self, self.j, self.one_or_all)
            self.main_GUI.signal1.connect(self.show_status)
            self.setCentralWidget(self.main_GUI)
            self.statusBar().showMessage('')

        self.main_GUI.signal_next[str].connect(self.show_object)
        self.main_GUI.signal_previous[str].connect(self.show_object)
        self.main_GUI.signal_jump[str].connect(self.show_object)
        self.main_GUI.signal_one_line[str].connect(self.one_line)
        self.main_GUI.plot()
        self.main_GUI.send_message()

        # self.setLayout(grid)
        self.setGeometry(0, 0, 1600, 900)
        self.setWindowTitle(field_name)
        self.setWindowIcon(QtGui.QIcon('icon.png'))
        self.show()

    def one_line(self, text_line):  # Switches between showing one line or all
        one_or_all = text_line
        self.one_or_all = one_or_all
        if self.one_or_all == 'one':
            self.j = high_objs[ID[self.j]]
        else:
            if self.j < len(ID)-1:
                self.j = lsd_cat_data[ID[self.j + 1]][0]
            else:
                self.j = len(ID)-1

    def show_object(self, text):  # Goes to next or previous object

        max_len = len(ID)

        ID_old = ID[self.j]
        ID_old_here = np.where(sorted(list(lsd_cat_data.keys())) == ID_old)[0][0]

        if text == 'next':
            forward_back = 1
        elif text == 'previous':
            forward_back = -1
        else:  # for jumping to object
            try:
                j_read = int(text)
                jump_h = np.where(np.asarray(sorted(list(lsd_cat_data.keys()))) == j_read)[0][0]
                forward_back = jump_h - ID_old_here
            except:
                forward_back = 0

        # if the end of the lines is not reached, but it's the last ID
        if self.j < max_len - 1 and ID_old == len(list(lsd_cat_data.keys())):
            # stay at this ID (the last)
            ID_new = ID_old
            # if we are going back, it's fine, we go back
            if text == 'previous':
                ID_new = sorted(list(lsd_cat_data.keys()))[-2]
            # if we are going forward and only looking at one line,
            # go to first object
            if text == 'next' and self.one_or_all == 'one':
                ID_new = 1
                self.went_through()
        elif self.j < max_len-1:
            ID_new = sorted(list(lsd_cat_data.keys()))[ID_old_here+forward_back]
        else:
            if text == 'next':
                self.went_through()
                ID_new = sorted(list(lsd_cat_data.keys()))[0]
            else:
                ID_new = sorted(list(lsd_cat_data.keys()))[-2]

        if self.one_or_all == 'one':
            self.j = high_objs[ID_new]
        else:
            self.j += forward_back
            if text != 'next' and text != 'previous':
                self.j = high_objs[ID_new]

        if self.j < 0:
            self.j = max_len-1
        if self.j >= max_len:
            self.went_through()

        self.main_GUI = main_GUI(self, self.j, self.one_or_all)
        self.main_GUI.signal1.connect(self.show_status)
        self.setCentralWidget(self.main_GUI)
        self.statusBar().showMessage('')
        self.main_GUI.signal_next[str].connect(self.show_object)
        self.main_GUI.signal_previous[str].connect(self.show_object)
        self.main_GUI.signal_jump[str].connect(self.show_object)
        self.main_GUI.signal_one_line[str].connect(self.one_line)
        self.signal_j.emit(self.j)
        self.main_GUI.plot()
        self.main_GUI.send_message()

    def show_status(self, message):
        self.statusBar().showMessage(message)

    def went_through(self):
        number_classified = len([a for a in lines_ident if a != '-' and
                                 a != 'Revisit'])
        reply = QtWidgets.QMessageBox.question(self, 'Message',
                                           "Congratulations, you went through all objects \nand classified " + str(number_classified)
                                           + "/" + str(len(lines_ident)) +
                                           " lines! " +
                                           "\nDo you want to quit?",
                                           QtWidgets.QMessageBox.StandardButton.Yes |
                                           QtWidgets.QMessageBox.StandardButton.No,
                                           QtWidgets.QMessageBox.StandardButton.No)
        if reply == QtWidgets.QMessageBox.StandardButton.Yes:
            self.close()
        self.j = -1


class VerticalLabel(QtWidgets.QLabel):

    def __init__(self, *args):
        QtWidgets.QLabel.__init__(self, *args)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.translate(0, self.height())
        painter.rotate(-90)
        # calculate the size of the font
        fm = QtGui.QFontMetrics(painter.font())
        xoffset = int(fm.boundingRect(self.text()).width()/2)
        yoffset = int(fm.boundingRect(self.text()).height()/2)
        x = int(self.width()/2) + yoffset - 5
        y = int(self.height()/2) - xoffset
        painter.drawText(y, x, self.text())
        painter.end()

    def minimumSizeHint(self):
        size = QtWidgets.QLabel.minimumSizeHint(self)
        return QtCore.QSize(size.height(), size.width())

    def sizeHint(self):
        size = QtWidgets.QLabel.sizeHint(self)
        return QtCore.QSize(size.height(), size.width())


class main_GUI(QtWidgets.QWidget):

    signal1 = QtCore.pyqtSignal(str)
    signal_next = QtCore.pyqtSignal(str)
    signal_previous = QtCore.pyqtSignal(str)
    signal_jump = QtCore.pyqtSignal(str)
    signal_one_line = QtCore.pyqtSignal(str)
    signal_j = QtCore.pyqtSignal(int)


    def __init__(self, parent, j, one_or_all):
        super(main_GUI, self).__init__()

        self.initUI(j, one_or_all)

    def initUI(self, j, one_or_all):

        grid = QtWidgets.QGridLayout()
        grid.setSpacing(10)

        message = 'None'
        self.message = message

        self.j = j
        self.one_or_all = one_or_all

        # check if you want automatic matching of lines
        # auto_button = QtWidgets.QPushButton('Auto Identify!', self)
        # auto_button.setToolTip('Automatically try to identify the lines for objects \nwith multiple detected emission lines.')
        # auto_button.clicked.connect(self.auto_ident)
        # grid.addWidget(auto_button, 1, 3, 1, 1)

        # create sub-grids
        plots_grid = QtWidgets.QGridLayout()
        id_grid = QtWidgets.QGridLayout()
        line_info_grid = QtWidgets.QGridLayout()
        slider_grid = QtWidgets.QGridLayout()
        navigation_grid = QtWidgets.QGridLayout()
        navigation_grid.setColumnStretch(0, 1)
        navigation_grid.setColumnStretch(1, 1)
        cuts_grid = QtWidgets.QGridLayout()

        title_guess = QtWidgets.QLabel('Initial guess:', self)
        id_grid.addWidget(title_guess, 0, 0, 1, 1)
        button_box = QtWidgets.QHBoxLayout()
        for i, name in enumerate(possible_lines_names):
            button = QtWidgets.QPushButton(name, self)
            button.clicked.connect(self.send_message)
            button.clicked.connect(self.plot)
            button.setMaximumWidth(60)
            button_box.addWidget(button)
        id_grid.addLayout(button_box, 0, 1, 1, 1)

        # add custom button
        custom_box = QtWidgets.QHBoxLayout()
        title_custom_button = QtWidgets.QLabel('Custom button', self)
        custom_box.addWidget(title_custom_button)
        self.grid = grid
        self.cust_button_main = QtWidgets.QPushButton(custom_button_label[0], self)
        self.cust_button_main.clicked.connect(self.cust_button_press)
        self.cust_button_main.setMaximumWidth(60)
        self.cust_button_main.setToolTip("Push when green.")
        if custom_button_label[0] != '?':
            self.cust_button_main.clicked.connect(self.send_message)
            self.cust_button_main.clicked.connect(self.plot)
        custom_box.addWidget(self.cust_button_main)

        self.lbdas_sorted = [np.where(np.asarray(list(lbda.values())) ==
                                      sorted(np.asarray(list(lbda.values())))
                                      [slbda])[0][0] for slbda in
                             range(len(np.asarray(list(lbda.keys()))))]
        self.lbda_sorted = np.asarray([list(lbda.keys())[ls] for ls in
                                       self.lbdas_sorted])

        self.combo_custom_button = QtWidgets.QComboBox(self)
        for possible_ident in self.lbda_sorted:
            self.combo_custom_button.addItem(possible_ident)
        self.combo_custom_button.setCurrentIndex(custom_combo_index[0])
        custom_box.addWidget(self.combo_custom_button)
        self.combo_custom_button.setToolTip("Pick your guess.")
        self.combo_custom_button.activated[int].connect(self.onActivated_ccb)
        id_grid.addLayout(custom_box, 1, 0, 1, 2)

        # comment line
        title_comm = QtWidgets.QLabel('Comments:', self)
        title_comm.setWordWrap(True)
        self.le = QtWidgets.QLineEdit(self)
        id_grid.addWidget(title_comm, 2, 0, 1, 1)
        id_grid.addWidget(self.le, 2, 1, 1, 1)

        # association line
        title_ass = QtWidgets.QLabel('  Association:', self)
        self.le_ass = QtWidgets.QLineEdit(self)
        custom_box.addWidget(title_ass)
        custom_box.addWidget(self.le_ass)

        id_box = QtWidgets.QHBoxLayout()

        # explain drop down for identification
        title_guess = QtWidgets.QLabel('Identification:', self)
        title_guess.setWordWrap(True)
        grid.addWidget(title_guess, 6, 6, 1, 1)
        self.drop_down_identification(grid, (6, 7, 1, 1))

        # explain drop down for Quality
        title_qual = QtWidgets.QLabel('Quality:', self)
        id_box.addWidget(title_qual)

        # Drop down for quality
        self.combo_qual = QtWidgets.QComboBox(self)
        self.combo_qual.addItem("-")
        self.combo_qual.addItem("a")
        self.combo_qual.addItem("b")
        self.combo_qual.addItem("c")
        self.combo_qual.setCurrentIndex(0)
        id_box.addWidget(self.combo_qual)
        self.combo_qual.setToolTip('Quality of redshift. \n a = multiple lines were detected (set automatically if other lines were also identified) \n b = one detected line and one other line visible \n c = only one detected line and no other line visible (e.g. Lya, also the default)')
        self.combo_qual.activated[int].connect(self.onActivated_qual)

        # explain drop down for Confidence
        title_conf = QtWidgets.QLabel('Confidence:', self)
        id_box.addWidget(title_conf)

        # Drop down for confidence
        self.combo_conf = QtWidgets.QComboBox(self)
        self.combo_conf.addItem("0")
        self.combo_conf.addItem("1")
        self.combo_conf.addItem("2")
        self.combo_conf.addItem("3")
        self.combo_conf.setCurrentIndex(0)
        id_box.addWidget(self.combo_conf)
        self.combo_conf.setToolTip("Confidence of identification. \n 0 = I don't know (use with caution) \n 1 = uncertain \n 2 = probably \n 3 = I'm sure \n The default for quality a is confidence 3.")
        self.combo_conf.activated[int].connect(self.onActivated_conf)

        # jump to specific object
        title_jump = QtWidgets.QLabel('Jump to ID:', self)
        self.le_jump = QtWidgets.QLineEdit(self)
        button_jump = QtWidgets.QPushButton('Jump', self)
        button_jump.setToolTip('Jump to ID')
        button_jump.clicked.connect(self.np_obj)
        button_jump.setShortcut('j')
        navigation_grid.addWidget(title_jump, 3, 0, 1, 1)
        navigation_grid.addWidget(self.le_jump, 3, 1, 1, 1)
        navigation_grid.addWidget(button_jump, 4, 0, 1, 2)



        sah = QtWidgets.QLabel('Total Objs: ' + str(total_num_obj)  + '\n' +
                               'Current Obj. ID ' + str(ID[self.j]) + '\n' +
                               'Unclassified Objs: ' + str(num_uncl[0]) + '\n' +
                               'Next unclassified obj. ID ' +
                               start_again_here_ID[0], self)

        sah.setWordWrap(True)
        sah.setMinimumHeight(60)
        navigation_grid.addWidget(sah, 2, 0, 1, 2)

        # set buttons for next or previous object
        np_name = {0: ['next', 'right'],
                   1: ['previous', 'left']}
        for i in range(2):
            button_np = QtWidgets.QPushButton('', self)
            button_np.setIcon(QtGui.QIcon(abspath+np_name[i][0]+'.png'))
            button_np.setToolTip('Look at '+np_name[i][0]+' object.')
            button_np.setText(np_name[i][0])
            button_np.clicked.connect(self.np_obj)
            button_np.setShortcut(np_name[i][1])
            if np_name[i][0] == 'previous':
                navigation_grid.addWidget(button_np, 1, 0, 1, 1)
            else:
                navigation_grid.addWidget(button_np, 1, 1, 1, 1)

        # check if you want only one line per object or many?
        cb = QtWidgets.QCheckBox('Show only strongest line', self)
        cb.setToolTip('Show only the strongest line \n(the one with the highest S/N) \nfor each detected object.')
        self.ID_here = ID[self.j]
        self.high_objs = high_objs
        if self.one_or_all == 'one':
            cb.toggle()
            self.index_here = self.high_objs[self.ID_here]
            self.j = self.index_here
        else:
            self.index_here = self.j
        cb.stateChanged.connect(self.changeObjs)
        navigation_grid.addWidget(cb, 0, 0, 1, 2)

        # information about line
        ID_old = ID[self.j]
        ID_old_here = np.where(sorted(list(lsd_cat_data.keys())) == ID_old)[0][0]

        title_ID = QtWidgets.QLabel('ID  ' + str(self.ID_here) + '    (number '
                                    + str(ID_old_here+1) + ' of ' +
                                    str(total_num_obj) + ' objects)', self)
        title_ID.setWordWrap(True)
        line_info_grid.addWidget(title_ID, 0, 0, 1, 2)

        line_look_here = np.where(self.index_here ==
                                  lsd_cat_data[self.ID_here])[0][0]
        title_num = QtWidgets.QLabel('Lines in obj:  ' +
                                     str(line_look_here + 1) + '/' +
                                     str(len(lsd_cat_data[self.ID_here])), self)
        self.info_line_label = dict()
        line_info_grid.addWidget(title_num, 1, 0, 1, 3)

        short_gone = 0
        for i, a in enumerate(info_line):
            if a != 'Short':
                self.title1 = QtWidgets.QLabel(a + ':', self)
                self.title2 = QtWidgets.QLabel(str(info_line[a][self.index_here]),
                                               self)
                self.title2.setWordWrap(True)
                line_info_grid.addWidget(self.title1, i+2, 0, 1, 1)
                line_info_grid.addWidget(self.title2, i+2, 1, 1, 2)
                self.info_line_label[a] = self.title2
            else:
                self.title2 = QtWidgets.QLabel(str(info_line[a][self.index_here]),
                                               self)
                self.info_line_label[a] = self.title2
                short_gone = 1

        # set min and max values for width and height of plots
        min_height = 100
        min_width = 100
        max_height = 200
        max_width = 200

        # HST cutout
        self.cutout_hst = pg.GraphicsLayoutWidget(self)
        self.cutout_hst.setMinimumSize(min_width, min_height)
        self.cutout_hst.setMaximumSize(max_width, max_height)
        grid.addWidget(self.cutout_hst, 10, 9, 7, 2)

        if show_hst_now[0]:
            hst_title = ('HST image:')
            hst_title_label = QtWidgets.QLabel(hst_title, self)
            grid.addWidget(hst_title_label, 10, 8, 1, 2)

            # cuts for the hst image
            self.cut_hst1 = np.round(cut_hst1[0], 6)
            self.cut_hst2 = np.round(cut_hst2[0], 6)

            # enter cuts
            cut_hst1_comm = QtWidgets.QLabel('HST Cut min', self)
            cuts_grid.addWidget(cut_hst1_comm, 1, 0, 1, 1)
            self.le_cut_hst1 = QtWidgets.QLineEdit(self)
            cuts_grid.addWidget(self.le_cut_hst1, 1, 1, 1, 1)
            self.le_cut_hst1.returnPressed.connect(self.show_hst)

            cut_hst2_comm = QtWidgets.QLabel('HST Cut max', self)
            cuts_grid.addWidget(cut_hst2_comm, 1, 2, 1, 1)
            self.le_cut_hst2 = QtWidgets.QLineEdit(self)
            cuts_grid.addWidget(self.le_cut_hst2, 1, 3, 1, 1)
            self.le_cut_hst2.returnPressed.connect(self.show_hst)

            self.le_cut_hst1.setText(str(self.cut_hst1))
            self.le_cut_hst2.setText(str(self.cut_hst2))

            # button_show = QtWidgets.QPushButton('Show', self)
            # button_show.clicked.connect(self.show_hst)
            # grid.addWidget(button_show, 11, 8, 1, 1)
            # button_hide = QtWidgets.QPushButton('Hide', self)
            # button_hide.clicked.connect(self.hide_hst)
            # grid.addWidget(button_hide, 12, 8, 1, 1)
            self.show_hst()

        # cuts for the data and s/n images
        self.cut_1 = cut_1[0]
        self.cut_2 = cut_2[0]
        self.cut_sn_1 = cut_sn_1[0]
        self.cut_sn_2 = cut_sn_2[0]

        # enter cuts for MUSE data
        cut1_comm = QtWidgets.QLabel('NB Cut min', self)
        cuts_grid.addWidget(cut1_comm, 2, 0, 1, 1)
        self.le_cut1 = QtWidgets.QLineEdit(self)
        cuts_grid.addWidget(self.le_cut1, 2, 1, 1, 1)
        self.le_cut1.returnPressed.connect(self.plot)

        cut2_comm = QtWidgets.QLabel('NB Cut max', self)
        cuts_grid.addWidget(cut2_comm, 2, 2, 1, 1)
        self.le_cut2 = QtWidgets.QLineEdit(self)
        cuts_grid.addWidget(self.le_cut2, 2, 3, 1, 1)
        self.le_cut2.returnPressed.connect(self.plot)

        self.le_cut1.setText(str(self.cut_1))
        self.le_cut2.setText(str(self.cut_2))

        sn_cut1_comm = QtWidgets.QLabel('S/N cut min', self)
        cuts_grid.addWidget(sn_cut1_comm, 3, 0, 1, 1)
        self.le_sn_cut1 = QtWidgets.QLineEdit(self)
        cuts_grid.addWidget(self.le_sn_cut1, 3, 1, 1, 1)
        self.le_sn_cut1.returnPressed.connect(self.plot)
        self.le_sn_cut1.setText(str(self.cut_sn_1))

        sn_cut2_comm = QtWidgets.QLabel('S/N cut max', self)
        cuts_grid.addWidget(sn_cut2_comm, 3, 2, 1, 1)
        self.le_sn_cut2 = QtWidgets.QLineEdit(self)
        cuts_grid.addWidget(self.le_sn_cut2, 3, 3, 1, 1)
        self.le_sn_cut2.returnPressed.connect(self.plot)
        self.le_sn_cut2.setText(str(self.cut_sn_2))

        # Enter width of the cutout windows
        self.width_window = width_window[0]
        width_window_comm = QtWidgets.QLabel('Size (")', self)
        cuts_grid.addWidget(width_window_comm, 0, 0, 1, 1)
        self.le_width_window = QtWidgets.QLineEdit(self)
        cuts_grid.addWidget(self.le_width_window, 0, 1, 1, 1)
        self.le_width_window.returnPressed.connect(self.plot)
        self.le_width_window.returnPressed.connect(self.show_hst)
        self.le_width_window.setText(str(self.width_window))

        # Write position (convert to hour)
        RA_sn_hour = Angle(RA_sn[self.j], unit=u.degree).hour
        RA_sn_hour1 = int(RA_sn_hour)
        RA_sn_hour2 = 60*(RA_sn_hour-RA_sn_hour1)
        RA_sn_hour3 = np.round(60*(RA_sn_hour2-int(RA_sn_hour2)), 5)
        RA_label1 = QtWidgets.QLabel('RA:', self)
        RA_label2 = QtWidgets.QLabel(str(RA_sn_hour1) + ':' +
                                     str(int(RA_sn_hour2)) + ':' +
                                     str(RA_sn_hour3), self)
        line_info_grid.addWidget(RA_label1, 10, 0, 1, 1)
        line_info_grid.addWidget(RA_label2, 10, 1, 1, 2)
        DEC_sn_hour = DEC_sn[self.j]
        DEC_sn_hour1 = int(DEC_sn[self.j])
        DEC_sn_hour2 = 60 * np.abs(DEC_sn_hour - DEC_sn_hour1)
        DEC_sn_hour3 = np.round(60 * (DEC_sn_hour2 - int(DEC_sn_hour2)), 5)
        DEC_label1 = QtWidgets.QLabel('DEC:', self)
        DEC_label2 = QtWidgets.QLabel(str(DEC_sn_hour1) + ':' +
                                      str(int(DEC_sn_hour2)) + ':' +
                                      str(DEC_sn_hour3), self)
        line_info_grid.addWidget(DEC_label1, 11, 0, 1, 1)
        line_info_grid.addWidget(DEC_label2, 11, 1, 1, 2)

        # Write S/N of the current line
        SN_label1 = QtWidgets.QLabel('S/N:', self)
        SN_label2 = QtWidgets.QLabel(str(np.round(detsn_max[self.j], 2)), self)
        line_info_grid.addWidget(SN_label1, 12, 0, 1, 2)
        line_info_grid.addWidget(SN_label2, 12, 1, 1, 2)

        # set explanation of rows
        row_expl = ['Spectrum', 'NB Image', 'S/N Image']
        for a in range(3):
            title = VerticalLabel(row_expl[a], self)
            plots_grid.addWidget(title, a, 0, 1, 1)

        # plots for lines
        self.plots_lines = dict()
        self.line_eds = dict()
        for x in range(0, 5):
            self.line = pg.GraphicsLayoutWidget(self)
            self.line.setMinimumSize(min_width, min_height)
            self.line.setMaximumSize(max_width, max_height)
            plots_grid.addWidget(self.line, 0, 1+x, 1, 1)
            self.plots_lines[x] = self.line
            self.line_ed = QtWidgets.QLabel(self)
            plots_grid.addWidget(self.line_ed, 3, 1+x, 1, 1)
            self.line_eds[x] = self.line_ed

        # MUSE cutouts
        self.plots_cutouts = dict()
        for x in range(0, 5):
            self.cutout = pg.GraphicsLayoutWidget(self)
            self.cutout.setMinimumSize(min_width, min_height)
            self.cutout.setMaximumSize(max_width, max_height)
            plots_grid.addWidget(self.cutout, 1, 1+x, 1, 1)
            self.plots_cutouts[x] = self.cutout

        # MUSE SN cutouts
        self.plots_cutouts_sn = dict()
        for x in range(0, 5):
            self.cutout_sn = pg.GraphicsLayoutWidget(self)
            self.cutout_sn.setMinimumSize(min_width, min_height)
            self.cutout_sn.setMaximumSize(max_width, max_height)
            plots_grid.addWidget(self.cutout_sn, 2, 1+x, 1, 1)
            self.plots_cutouts_sn[x] = self.cutout_sn

        # set slider smooth
        self.smooth_more = smooth_more[0]
        sld_smooth = QtWidgets.QSlider(QtCore.Qt.Orientation.Horizontal, self)
        sld_smooth.setMinimumWidth(180)
        slider_grid.addWidget(sld_smooth, 3, 1, 1, 1)
        sld_smooth.setRange(0, 5)
        sld_smooth.setValue(self.smooth_more)
        sld_smooth.setSingleStep(1)
        sld_smooth.valueChanged[int].connect(self.changeValue2)

        # set labels for slider
        labels_smooth = np.arange(0, 6, 1)
        str_label = ' ' + str(labels_smooth[0])
        for lab in range(len(labels_smooth[1:])):
            str_label += '   ' + str(labels_smooth[lab+1])
        title_smooth_label = QtWidgets.QLabel(str_label, self)
        slider_grid.addWidget(title_smooth_label, 4, 1, 1, 1)
        title_smooth = QtWidgets.QLabel('Slide to smooth!', self)
        slider_grid.addWidget(title_smooth, 2, 1, 1, 1)
        sld_smooth.setToolTip('Smoothes with a Gaussian filter with this sigma.')

        # set slider zoom in spectrum
        self.zoom = zoom[0]
        sld_zoom = QtWidgets.QSlider(QtCore.Qt.Orientation.Horizontal, self)
        slider_grid.addWidget(sld_zoom, 3, 2, 1, 1)
        sld_zoom.setRange(1, 20)
        sld_zoom.setValue(self.zoom)
        sld_zoom.setSingleStep(1)
        sld_zoom.valueChanged[int].connect(self.changeValue_zoom)
        title_zoom2 = QtWidgets.QLabel('Slide to zoom!', self)
        slider_grid.addWidget(title_zoom2, 2, 2, 1, 1)
        sld_zoom.setToolTip('Zoom!')

        # set slider aperture
        self.change_ap = ap[0]
        sld_ap = QtWidgets.QSlider(QtCore.Qt.Orientation.Horizontal, self)
        slider_grid.addWidget(sld_ap, 3, 3, 1, 1)
        sld_ap.setFixedWidth(195)
        sld_ap.setRange(1, 10)
        sld_ap.setValue(int(self.change_ap*2))
        sld_ap.setSingleStep(1)
        sld_ap.valueChanged[int].connect(self.changeAp)

        # set labels for slider
        label_ap = np.arange(1, 6, 1)
        str_label = '   ' + str(label_ap[0])
        for lab in range(len(label_ap[1:])):
            str_label += '    ' + str(label_ap[lab+1])
        title_ap_label = QtWidgets.QLabel(str_label, self)
        slider_grid.addWidget(title_ap_label, 4, 3, 1, 1)
        title_ap = QtWidgets.QLabel('Slide to change aperture (in arcsec)!', self)
        slider_grid.addWidget(title_ap, 2, 3, 1, 1)
        sld_ap.setToolTip('Changes the aperture used to getting the spectrum.')

        # set y-range
        self.yrange = yrange[0]
        sld_yrange = QtWidgets.QSlider(QtCore.Qt.Orientation.Vertical, self)
        slider_grid.addWidget(sld_yrange, 0, 0, 1, 1)
        sld_yrange.setRange(100, 420)
        sld_yrange.setValue(int(np.rint(np.log10(self.yrange)*100)))
        sld_yrange.setSingleStep(5)
        sld_yrange.valueChanged[int].connect(self.changeValue_yrange)
        sld_yrange.setToolTip('Change the y-axis range.')

        self.diff_red_line = [0]

        # set full spectrum
        self.full_spec = pg.GraphicsLayoutWidget(self)
        self.full_spec.setMinimumWidth(min_width*5+10*4+20)
        self.full_spec.setMinimumHeight(min_height)
        slider_grid.addWidget(self.full_spec, 0, 1, 2, 3)

        # add sub-grids to main grid
        grid.addLayout(plots_grid, 0, 0, 15, 6)
        grid.addLayout(id_grid, 0, 6, 6, 5)
        grid.addLayout(id_box, 6, 8, 1, 3)
        grid.addLayout(line_info_grid, 7, 6, 9, 3)
        grid.addLayout(navigation_grid, 7, 9, 1, 2)
        grid.addLayout(slider_grid, 15, 0, 6, 6)
        grid.addLayout(cuts_grid, 17, 6, 4, 4)

        self.setLayout(grid)

    def drop_down_identification(self, grid, pos):
        # Drop down for identification
        line_ident = "-"  # default for identification
        self.line_ident = line_ident
        self.combo = QtWidgets.QComboBox(self)
        for possible_ident in possible_identifications:
            self.combo.addItem(possible_ident)

            self.combo.setToolTip("Please select an identification.\nOther found lines will automatically\nbe assigned an identification\nthat matches the redshift.\n\n- Lya = Lyman alpha\n- OII = OII doublet\n- OIII 2 = stronger OIII2 line at 5008A\n- Ha = H alpha\n- Something else = real line that is either not in line list\n                        (if you know what it is, write in comment)\n                        or cannot be identified\n- Continuum residual = (median filter) residual of either\n                        a star or a galaxy, not a real emission line\n- Edge = a real object/line that is at the edge\n                        and cannot be identified\n- Crap = this is not real (e.g. sky residual)\n- Revisit = should be avoided in the end\n- OIII 1 = the fainter OIII line at 4960A\n- Hb = H beta")

        self.combo.setCurrentIndex(0)
        grid.addWidget(self.combo, pos[0], pos[1], pos[2], pos[3])
        self.combo.activated.connect(self.onActivated)

    def next_unclass(self):

        start_again = [str(i) for i in range(len(lines_ident)) if
                       lines_ident[i] == '-' or lines_ident[i] == 'Revisit']
        if len(start_again) == 0:
            start_again_here[0] = 'None'
            start_again_here_ID[0] = 'None'
            num_uncl[0] = 0
        else:
            start_again_here[0] = start_again[0]
            try:
                unclassified = np.asarray([int(i) for i in start_again if
                                                int(i) in high_objs.values()])
                idx = np.where(unclassified > self.j)[0][0]
            except ValueError:
                idx = 0
            except IndexError:
                idx = 0
            start_again_here_ID[0] = str(ID_first[int(unclassified[idx])])
            num_uncl[0] = len(unclassified)

    def save_now(self):
        write_output(catalog, lines_ident, lines_comment, lines_redshifts,
                     lines_quality, lines_confidence, poss_ident_short,
                     lines_assoc, new_catalog)

    def hide_hst(self):
        self.cutout_hst.clear()
        show_hst_now[0] = False

    def show_hst(self):

        hst_ap = ap[0]/hst_ps

        self.cut_hst1 = float(self.le_cut_hst1.text())
        self.cut_hst2 = float(self.le_cut_hst2.text())
        cut_hst1[0] = self.cut_hst1
        cut_hst2[0] = self.cut_hst2

        xy_hst = radec_to_pix([RA_sn], [DEC_sn], hst_header)
        x_hst = xy_hst[0][0]
        y_hst = xy_hst[1][0]
        idx = self.index_here

        self.cutout_hst.clear()
        show_hst_now[0] = True

        img_hst = pg.ImageItem(border='k')
        if des == 'old':
            img_hst.setLookupTable(cubehelix(ch_g, ch_s, ch_r, ch_h))
        else:
            img_hst.setLookupTable(viridis)
        img_hst.setImage(np.rot90(hst_data)[::-1])
        img_hst.setLevels([cut_hst1[0], cut_hst2[0]])
        self.view_hst = self.cutout_hst.addViewBox()
        self.view_hst.addItem(img_hst)
        width_window_pix_hst = width_window[0] / hst_ps
        self.view_hst.setRange(QtCore.QRectF(x_hst[idx]-width_window_pix_hst/2.,
                                             y_hst[idx]-width_window_pix_hst/2.,
                                             width_window_pix_hst,
                                             width_window_pix_hst))

        self.view_hst.setAspectLocked(True)

        test_line = pg.InfiniteLine(angle=90, movable=False,
                                    pen=pg.mkPen(color=c_hstline, width=1))
        test_line.setPos([x_hst[idx], 0])
        test_line2 = pg.InfiniteLine(angle=0, movable=False,
                                     pen=pg.mkPen(color=c_hstline, width=1))
        test_line2.setPos([0, y_hst[idx]])

        strongest_lines = [high_objs[i] for i in high_objs.keys() if i != idx]

        for aux in strongest_lines:
            reg = pg.CircleROI([x_hst[aux]-hst_ap/2., y_hst[aux]-hst_ap/2.],
                                [hst_ap, hst_ap], pen=('c'), movable=False,
                                resizable=False)
            reg_id = pg.TextItem(str(data['ID'][aux]), color='c')
            self.view_hst.addItem(reg)
            self.view_hst.addItem(reg_id)
            reg_id.setPos(x_hst[aux], y_hst[aux])

        pen = pg.mkPen('m', width=2)
        roi = pg.CircleROI([x_hst[idx] - hst_ap/2.,
                            y_hst[idx] - hst_ap/2.],
                            [hst_ap, hst_ap], pen=pen, movable=False)

        self.view_hst.addItem(test_line)
        self.view_hst.addItem(test_line2)
        self.view_hst.addItem(roi)

    def changeObjs(self, state):

        try:
            aux_check = QtCore.Qt.CheckState.Checked.value
        except AttributeError:
            aux_check = QtCore.Qt.CheckState.Checked

        if state == aux_check:
            sender_one = 'one'
        else:
            sender_one = 'all'
        self.signal_one_line.emit(sender_one)

    def auto_ident(self):
        # Automatic identification.
        lines_lam_all = np.asarray(list(lbda.values()))
        high_objs.keys()
        # go trough all IDs
        for high_sn_ID in list(high_objs.keys()):
            high_sn_pos = high_objs[high_sn_ID]
            wavel_here = [lam_sn[high_sn_pos]][0]
            # only use highest sn line for identification
            act_pos = lsd_cat_data[high_sn_ID]
            # if more than one line found, try to identify by going through all
            # possible identifications and find option with most matches
            if len(lsd_cat_data[high_sn_ID]) > 1:
                matches_number = []
                matches_red = []
                matches_ident = []
                for try_line_name in list(lbda.keys()):
                    matches_num = 0
                    match_red = []
                    match_ident = []
                    # convert found wavelength in air to vacuum
                    self.redshift = air_to_vac(wavel_here) / lbda[try_line_name] - 1.
                    z_lam_all = (self.redshift + 1) * air_to_vac(lines_lam_all)
                    other_found_lines_lam = air_to_vac(z_sn[lsd_cat_data[high_sn_ID]]
                                                       * wavelength_pp +
                                                       start_wavelength)
                    for iline, fline in enumerate(other_found_lines_lam):
                        diff_all = abs(z_lam_all-fline)
                        here_pos = lsd_cat_data[high_sn_ID][iline]
                        if np.min(diff_all) < 5 and self.redshift > 0:
                            matches_num += 1
                            closest = np.where(diff_all == np.min(diff_all))[0][0]
                            match_ident.append(list(lbda.keys())[closest])
                            match_red.append((np.round(
                                self.redshift, 5)))
                        else:
                            match_ident.append("no match")
                            match_red.append(0.)

                    matches_number.append(matches_num)
                    matches_red.append(match_red)
                    matches_ident.append(match_ident)
                matched_most = np.where(matches_number ==
                                        np.max(matches_number))[0][0]
                for iline, fline in enumerate(other_found_lines_lam):
                    if np.max(matches_number) == len(other_found_lines_lam):
                        lines_ident[act_pos[iline]] = matches_ident \
                            [matched_most][iline]
                        lines_redshifts[act_pos[iline]] = matches_red \
                            [matched_most][iline]
                    if np.max(matches_number) != len(other_found_lines_lam):
                        # make sure not to write nonsense in case the matching
                        # didn't work
                        lines_quality[act_pos[iline]] = 'c'
                        lines_confidence[act_pos[iline]] = '0'
                        lines_redshifts[act_pos[iline]] = '0'
                        lines_ident[act_pos[iline]] = 'no match'
                        lines_short[act_pos[iline]] = '-'
                    else:
                        lines_quality[act_pos[iline]] = 'a'
                        lines_confidence[act_pos[iline]] = '3'

    def redshift_negative(self):
        reply = QtWidgets.QMessageBox.question(self, 'Message',
                                           "Attention, your identification is wrong, since the redshift would be negative.", QtWidgets.QMessageBox.StandardButton.Ok)

    def onActivated_ccb(self):
        text = self.combo_custom_button.currentText()
        self.cust_button_main.setText(text)
        custom_button_label[0] = str(text)
        custom_combo_index[0] = self.combo_custom_button.currentIndex()
        self.cust_button_main.clicked.connect(self.send_message)
        self.cust_button_main.clicked.connect(self.plot)
        try:
            col = '#%02x%02x%02x' % tuple(255*x for x in viridis_green[4])
        except:
            col = 'green'
        self.cust_button_main.setStyleSheet('QPushButton {background-color: ' + str(col) + '; color: black;}')

    def onActivated(self,  idx):
        self.line_ident = possible_identifications[idx]
        comment = self.le.text()
        if len(comment) > 1:
            lines_comment[self.index_here] = str(comment)

        assoc = self.le_ass.text()
        if len(assoc) != 0:
            lines_assoc[self.index_here] = str(assoc)

        act_pos = lsd_cat_data[self.ID_here]
        try:
            # convert found wavelength in air to vacuum
            self.redshift = air_to_vac(self.wavel_change) / lbda[self.line_ident] - 1.
            if self.redshift < 0:
                self.redshift_negative()
                lines_quality[self.index_here] = '-'
                lines_confidence[self.index_here] = '0'
                lines_redshifts[self.index_here] = 0.0
                lines_ident[self.index_here] = '-'
                lines_short[self.index_here] = '-'
                self.redshift = 0.0
                self.combo.setCurrentIndex(0)

            else:
                ### Try to identify possible other lines in the spectrum
                lines_ident[self.index_here] = self.line_ident
                bad = False
                lines_lam_all = np.asarray(list(lbda.values()))
                z_lam_all = (self.redshift+1)*lines_lam_all
                # Convert found emission lines to vacuum
                other_found_lines_lam = air_to_vac((z_sn[lsd_cat_data[self.ID_here]])*wavelength_pp+start_wavelength)

                for iline, fline in enumerate(other_found_lines_lam):
                    diff_all = abs(z_lam_all-fline)
                    here_pos = lsd_cat_data[self.ID_here][iline]
                    if np.min(diff_all) < 5:
                        closest = np.where(diff_all == np.min(diff_all))[0][0]
                        lines_ident[act_pos[iline]] = list(lbda.keys())[closest]
                        lines_short[act_pos[iline]] = poss_ident_short[list(lbda.keys())[closest]]
                        lines_redshifts[act_pos[iline]] = (np.round(
                            self.redshift, 5))
                    else:
                        lines_redshifts[act_pos[iline]] = 0.
                        lines_ident[act_pos[iline]] = 'no match'
                        lines_short[act_pos[iline]] = '-'
                        bad = True

                for iline, fline in enumerate(other_found_lines_lam):
                    if bad == True or len(other_found_lines_lam) == 1:
                        lines_quality[act_pos[iline]] = 'c'
                        lines_confidence[act_pos[iline]] = '0'
                    else:
                        lines_quality[act_pos[iline]] = 'a'
                        lines_confidence[act_pos[iline]] = '3'

        except KeyError:

            for iline in range(len(z_sn[lsd_cat_data[self.ID_here]])):
                if lines_ident[act_pos[iline]] in ['no match', '-']:
                    lines_ident[act_pos[iline]] = self.line_ident
                    lines_short[act_pos[iline]] = '-'
                    lines_quality[act_pos[iline]] = 'c'
                else:
                    lines_ident[act_pos[iline]] = self.line_ident
                    lines_short[act_pos[iline]] = poss_ident_short[self.line_ident]+'\t\t'
                    lines_quality[act_pos[iline]] = 'c'
                    lines_confidence[act_pos[iline]] = '0'

            lines_ident[self.index_here] = self.line_ident
            lines_short[self.index_here] = poss_ident_short[self.line_ident]+'\t\t'

        if self.line_ident not in np.asarray(list(lbda.keys())):
            lines_redshifts[self.index_here] = 0.0

        # Put updated information about line on GUI
        for i, a in enumerate(info_line):
            info_t = list(info_line.keys())[i]

            label_text = (str(info_line[info_t][self.index_here]))
            self.info_line_label[a].setText(label_text)

        self.info_line_label['Comment'].setWordWrap(True)

    def onActivated_qual(self):
        self.line_qual = self.combo_qual.currentText()
        lines_quality[self.index_here] = self.line_qual
        form_qual = (str(lines_quality[self.index_here]))
        self.info_line_label['Quality'].setText(form_qual)

        # if quality is set to a for one line, do so for the rest, too
        act_pos = lsd_cat_data[self.ID_here]
        other_found_lines_lam = air_to_vac((z_sn[lsd_cat_data[self.ID_here]]) *
                                           wavelength_pp + start_wavelength)
        lines_all = np.asarray(list(lbda.keys()))
        if self.line_qual == 'a':
            form_conf = ('3')
            self.info_line_label['Confidence'].setText(form_conf)
            lines_confidence[self.index_here] = '3'
            for iline, fline in enumerate(other_found_lines_lam):
                if lines_ident[act_pos[iline]] in lines_all:
                    lines_quality[act_pos[iline]] = 'a'
                    lines_confidence[act_pos[iline]] = '3'

            # Don't allow to set something to 'a' that has only one detected line
            if len(other_found_lines_lam) == 1:
                lines_quality[self.index_here] = 'c'

    def onActivated_conf(self):
        self.line_conf = self.combo_conf.currentText()
        lines_confidence[self.index_here] = self.line_conf
        form_conf = (str(lines_confidence[self.index_here]))
        self.info_line_label['Confidence'].setText(form_conf)

    def np_obj(self, parent):

        # Save comment
        comment = self.le.text()
        if len(comment) != 0:
            lines_comment[self.index_here] = str(comment)

        assoc = self.le_ass.text()
        if len(assoc) != 0:
            lines_assoc[self.index_here] = str(assoc)

        jump_id = self.le_jump.text()

        sender_np = self.sender()

        self.save_now()
        self.next_unclass()

        if sender_np.text() == 'previous':
            self.signal_previous.emit(sender_np.text())
        elif sender_np.text() == 'next':
            self.signal_next.emit(sender_np.text())
        else:
            self.signal_jump.emit(jump_id)

        try:
            self.redshift = air_to_vac(self.wavel_change) / lbda[self.line_ident] - 1.
        except KeyError:
            self.redshift = lines_redshifts[self.index_here]

        lines_redshifts[self.index_here] = np.round(self.redshift, 5)

    def changeValue2(self, value):
        self.smooth_more = value
        smooth_more[0] = value
        self.plot()

    def changeAp(self, value):
        self.change_ap = value/2.
        ap[0] = value/2.
        self.plot()
        if show_hst_now[0] == True:
            self.show_hst()

    def changeValue_zoom(self, value):
        self.zoom = value
        zoom[0] = value
        self.plot()

    def changeValue_yrange(self, value):
        self.yrange = value
        # to get logarithmic scaling
        yrange[0] = 10**(value/100.)
        self.plot()

    def cust_button_press(self):
        text = self.cust_button_main.text()
        if str(text) == '?':
            print('Please select a line from the dropdown first.')
        else:
            other_lines = []
            ci = self.combo_custom_button.currentIndex()
            if ci < 2:
                other_lines = self.lbda_sorted[:5]
            elif ci > len(self.lbda_sorted) - 3:
                other_lines = self.lbda_sorted[-5:]
            else:
                new_ind = ci-2
                for c in range(5):
                    other_lines.append(self.lbda_sorted[new_ind])
                    new_ind += 1
            dict_other_lines[str(text)] = other_lines
            if self.lbda_sorted[ci] not in possible_identifications:
                possible_identifications.append(self.lbda_sorted[ci])
        self.drop_down_identification(self.grid, (6, 7, 1, 1))

    def send_message(self):

        self.diff_red_line = [0]

        sender = self.sender()
        try:
            self.message = sender.text()
        except AttributeError:
            self.message = str(info_line['Identification'][self.index_here])
            if self.message not in possible_lines_names:
                self.message = 'Lya'
        self.signal1.emit(self.message)

        # set explanation of columns
        self.guess_line_name = str(self.message)

        guess_line_lam = lbda[self.guess_line_name]
        other_lines_names = dict_other_lines[self.guess_line_name]

        for nam in range(5):
            try:
                self.line_eds[nam].setText(other_lines_names[nam])
                self.line_eds[nam].move(185+nam*200, 560)
                self.line_eds[nam].show()
            except:
                self.line_eds[nam].setText(' ')
                self.line_eds[nam].move(185+nam*200, 560)
                self.line_eds[nam].show()

    def plot(self):

        ### Create the Spectrum
        self.x_pos = [x_sn[self.index_here]]
        self.y_pos = [y_sn[self.index_here]]
        self.lam_pos = [lam_sn[self.index_here]]
        self.spec = spec_in_ap(self.x_pos[0], self.y_pos[0],
                               cube_data, 'sum', ap[0], pix_size)
        if noise_cube == True:
            self.noise = spec_in_ap(self.x_pos[0], self.y_pos[0],
                                    cube_noise_data, 'mean', ap[0], pix_size)
        if eff_noise_file != False:
            # scale the effective noise with the aperture
            # use standard deviation
            # sigma = sqrt(N*var), N=number of pixels
            self.noise = np.sqrt(eff_noise_spec)*np.sqrt(np.pi)*(ap[0]/pix_size)

        self.wavel_change = self.lam_pos[0]

        if str(self.message) not in all_lines_names:
            self.message = str(info_line['Identification'][self.index_here])
            if str(self.message) not in possible_lines_names:
                self.message = 'Lya'

        guess_line_name = str(self.message)
        guess_line_lam = lbda[guess_line_name]

        # get redshift for the guessed line and the other possible lines

        z = np.asarray(self.lam_pos[0]/guess_line_lam-1.)
        lines = np.asarray(dict_other_lines[guess_line_name])
        lines_lam = np.asarray(sorted(list({k: lbda[k] for k
                                            in lines}.values())))
        lines_lam_all = np.asarray(list(lbda.values()))

        z_lam = (z+1)*lines_lam
        z_lam_all = (z+1)*lines_lam_all
        z_pix = np.round((z_lam-start_wavelength)/wavelength_pp)
        z_pix_all = np.round((z_lam_all-start_wavelength)/wavelength_pp)

        # Make sure not to run into index errors
        while len(z_pix) <= len(dict_other_lines[guess_line_name]):
            z_pix = np.concatenate((z_pix, [0]))

        ### Smooth the spectrum
        spec_smooth = scipy.ndimage.filters.gaussian_filter(self.spec,
                                                            self.smooth_more,
                                                            order=0,
                                                            output=None,
                                                            mode='reflect',
                                                            cval=0.0,
                                                            truncate=4.0)

        ### Set the full spectrum
        self.full_spec.clear()
        pfs = self.full_spec.addPlot()
        zline = pg.InfiniteLine(angle=0, movable=False, pen=c_p1)  # (0, 250, 50, 90))

        # Add other found lines in this object
        lines_Pen2 = pg.mkPen(color=c_otherfoundlines, width=4, alpha=0.7)
        other_found_lines_lam = (z_sn[lsd_cat_data[self.ID_here]] *
                                 wavelength_pp + start_wavelength)
        for j in range(len(other_found_lines_lam)):
            vLines_all = pg.InfiniteLine(angle=90, movable=False,
                                         pen=lines_Pen2)
            vLines_all.setPos(other_found_lines_lam[j])
            pfs.addItem(vLines_all, ignoreBounds=True)

        pfs.plot(wavelength, spec_smooth, pen=c_spec)
        pfs.addItem(zline, ignoreBounds=True)
        pfs.setYRange(-yrange[0]/4., yrange[0]*(3./4.))
        pfs.setXRange(wavelength[0], wavelength[-1])
        # pfs.hideAxis('left')

        # lines for all other lines
        lines_Pen = pg.mkPen(color=c_otherlines, width=1)
        for j in range(len(z_lam_all)):
            vLines_all = pg.InfiniteLine(angle=90, movable=False,
                                         pen=lines_Pen)
            vLines_all.setPos(z_lam_all[j])
            pfs.addItem(vLines_all, ignoreBounds=True)
            line_name_text = np.asarray(list(lbda.keys()))[j]
            annotate2 = pg.TextItem(text=line_name_text, color=c_annotate,
                                    anchor=(1, 1), angle=-90)
            annotate2.setPos(z_lam_all[j], 100)
            pfs.addItem(annotate2)

        #####################################################################
        ### Make plots for 1st line

        self.view = dict()
        self.view_sn = dict()
        img = dict()
        self.img = img
        img_sn = dict()
        self.img_sn = img_sn
        region = dict()
        p2 = dict()
        rgn_x = dict()
        rgn_y = dict()
        pos = dict()
        minX = dict()
        maxX = dict()
        mousePoint = dict()
        vLine = dict()
        hLine = dict()
        vbs = dict()
        look_here_lam = dict()
        look_here = dict()
        line_pos_line = dict()
        line_zero_line = dict()
        vLines_all_all = dict()

        self.cut_1 = float(self.le_cut1.text())
        self.cut_2 = float(self.le_cut2.text())
        self.cut_sn_1 = float(self.le_sn_cut1.text())
        self.cut_sn_2 = float(self.le_sn_cut2.text())
        cut_1[0] = self.cut_1
        cut_2[0] = self.cut_2
        cut_sn_1[0] = self.cut_sn_1
        cut_sn_2[0] = self.cut_sn_2

        self.width_window = float(self.le_width_window.text())
        width_window[0] = self.width_window

        def set_red(val_change):
            val_change_num = val_change.pos()[0]
            self.wavel_change = val_change_num
            self.diff_red_line[0] = val_change_num - self.lam_pos[0]
            for i in range(len(self.plots_lines)):
                for j in z_lam_all:
                    try:
                        vLines_all_all[i][j].setPos(j + self.diff_red_line[0])
                    except KeyError:
                        tried = True

        def updateDataImg(i, here):
            ## Display the data
            if here < 0:
                here = 0
            try:
                self.img[i].setImage(np.rot90(cube_data[int(here)])[::-1])
                self.img_sn[i].setImage(np.rot90(cube_data_sn[int(here)])[::-1])
                if des == 'old':
                    self.img[i].setLookupTable(cubehelix(ch_g, ch_s, ch_r, ch_h))
                    self.img_sn[i].setLookupTable(rainbow3_file_data)
                else:
                    self.img[i].setLookupTable(viridis)
                    self.img_sn[i].setLookupTable(magma)
                self.img[i].setLevels([self.cut_1, self.cut_2])
                self.img_sn[i].setLevels([self.cut_sn_1, self.cut_sn_2])
            except KeyError:
                # Apparently we need something to happen after try...
                # This is needed for changing all windows by moving the mouse
                # over the spectrum of the found line. If looking at Ha and
                # there are no other lines, the signal is still sent although
                # there is no window to change. This raises a KeyError.
                tried = True
            except IndexError:
                pass

        def update(i):
            region[i].setZValue(10)
            minX[i], maxX[i] = region[i].getRegion()
            p2[i].setXRange(minX[i], maxX[i], padding=0)
            middle_minmax = minX[i] + (maxX[i] - minX[i]) / 2.
            middle_minmax = int(np.round((middle_minmax -
                                          start_wavelength) / wavelength_pp))
            updateDataImg(i, middle_minmax)

        def updateRegion(i, window, viewRange):
            rgn_x[i] = viewRange[0]
            rgn_y[i] = viewRange[1]
            region[i].setRegion(rgn_x[i])

        def mouseMoved(i, evt):
            pos[i] = evt
            if p2[i].sceneBoundingRect().contains(pos[i]):
                mousePoint[i] = vbs[i].mapSceneToView(pos[i])
                vLine[i].setPos(mousePoint[i].x())
                hLine[i].setPos(mousePoint[i].y())
                look_here_lam[i] = mousePoint[i].x()
                look_here[i] = int(np.round((look_here_lam[i] -
                                             start_wavelength) / wavelength_pp))
                updateDataImg(i, look_here[i])

        border = 0
        for i in range(len(self.plots_lines)):

            self.plots_lines[i].clear()
            self.plots_cutouts[i].clear()
            self.plots_cutouts_sn[i].clear()

            if z_pix[i] > border and z_pix[i] < len(wavelength) - border:

                self.plots_lines[i].show()
                self.plots_cutouts[i].show()
                self.plots_cutouts_sn[i].show()

                ### make the cutouts
                self.plots_cutouts[i].clear()
                self.view[i] = self.plots_cutouts[i].addViewBox()
                self.plots_cutouts_sn[i].clear()
                self.view_sn[i] = self.plots_cutouts_sn[i].addViewBox()

                ## Create image item

                borderPen = pg.mkPen(color=c_spec, width=3)

                if lines[i] in self.message:
                    img_sn[i] = pg.ImageItem(border=borderPen)
                    img[i] = pg.ImageItem(border=borderPen)
                elif 'OIII 2' in self.message and lines[i] == 'OIII 2':
                    img_sn[i] = pg.ImageItem(border=borderPen)
                    img[i] = pg.ImageItem(border=borderPen)
                else:
                    img_sn[i] = pg.ImageItem(border='k')
                    img[i] = pg.ImageItem(border='k')

                self.view[i].addItem(img[i])

                ## Set initial view bounds
                width_window_pix = width_window[0] / pix_size
                self.view[i].setRange(QtCore.QRectF(self.x_pos[0] -
                                                    width_window_pix/2.,
                                                    self.y_pos[0] -
                                                    width_window_pix/2.,
                                                    width_window_pix,
                                                    width_window_pix))

                self.view_sn[i].addItem(img_sn[i])
                self.view_sn[i].setRange(QtCore.QRectF(self.x_pos[0] -
                                                       width_window_pix/2.,
                                                       self.y_pos[0] -
                                                       width_window_pix/2.,
                                                       width_window_pix,
                                                       width_window_pix))

                ## lock the aspect ratio so pixels are always square
                self.view[i].setAspectLocked(True)
                self.view_sn[i].setAspectLocked(True)

                ## image, rotate to get correct orientation
                img[i].setImage(np.rot90(cube_data[int(z_pix[i])])[::-1])
                img_sn[i].setImage(np.rot90(cube_data_sn[int(z_pix[i])])[::-1])

                # determines the style of the zero line and highlighting
                myPen = pg.mkPen(color=c_p1)
                myPen2 = pg.mkPen(color=c_spec, width=2)
                myPen_noise = pg.mkPen(color=c_noise, width=1)

                self.plots_lines[i].clear()
                p2[i] = self.plots_lines[i].addPlot()
                p2[i].plot(x=wavelength, y=spec_smooth, pen=c_spec)
                if noise_cube == True or eff_noise_file != False:
                    p2[i].plot(x=wavelength, y=self.noise, pen=myPen_noise)
                p2[i].setYRange(-yrange[0]/4., yrange[0]*(3./4.), padding=0)
                region_highlight_brush = pg.mkBrush(c_region1)
                region_highlight_brush2 = pg.mkBrush(c_region2)

                region[i] = pg.LinearRegionItem()
                region[i].setZValue(z_lam[i])

                region[i].sigRegionChanged.connect(functools.partial(update, i))

                p2[i].sigRangeChanged.connect(functools.partial(updateRegion, i))
                region[i].setRegion([z_lam[i]-self.zoom*10/2.,
                                     z_lam[i]+self.zoom*10/2.])

                # line at position of the found line

                if lines[i] == self.message:
                    i_line = i
                    line_pos_line[i] = pg.InfiniteLine([z_lam[i] +
                                                       self.diff_red_line[0], 0],
                                                       angle=90,
                                                       movable=True, pen='r')

                    line_pos_line[i].sigPositionChanged.connect(set_red)
                    p2[i].addItem(line_pos_line[i], ignoreBounds=True)

                # line at zero
                line_zero_line[i] = pg.InfiniteLine(0, angle=0,
                                                    movable=False,  pen=myPen)
                p2[i].addItem(line_zero_line[i], ignoreBounds=True)

                vLines_all = dict()
                # lines for all other lines
                for j in range(len(z_lam_all)):
                    vLines_all[z_lam_all[j]] = pg.InfiniteLine(angle=90,
                                                               movable=False,
                                                               pen=lines_Pen)
                    vLines_all[z_lam_all[j]].setPos([z_lam_all[j] +
                                                     self.diff_red_line[0], 0])
                    p2[i].addItem(vLines_all[z_lam_all[j]], ignoreBounds=True)

                    line_name_text = np.asarray(list(lbda.keys()))[j]
                    annotate2 = pg.TextItem(text=line_name_text,
                                            color=c_annotate,
                                            anchor=(1, 1),
                                            angle=-90)
                    annotate2.setPos(z_lam_all[j], 500)
                    p2[i].addItem(annotate2)

                # Add other found lines in this object
                lines_Pen3 = pg.mkPen(color=c_otherfoundlines, width=2)
                other_found_lines_lam = (z_sn[lsd_cat_data[self.ID_here]] *
                                         wavelength_pp + start_wavelength)
                for j in range(len(other_found_lines_lam)):
                    vLines_all_other = pg.InfiniteLine(angle=90, movable=False,
                                                       pen=lines_Pen3)
                    vLines_all_other.setPos(other_found_lines_lam[j])
                    p2[i].addItem(vLines_all_other, ignoreBounds=True)

                vLines_all_all[i] = vLines_all

                # cross hair
                vLine[i] = pg.InfiniteLine(angle=90, movable=False, pen=c_ch)
                hLine[i] = pg.InfiniteLine(angle=0, movable=False, pen=c_ch)
                p2[i].addItem(vLine[i], ignoreBounds=True)
                p2[i].addItem(hLine[i], ignoreBounds=True)
                p2[i].hideAxis('left')

                # highlight the guessed line
                if lines[i] == self.message:
                    p2[i].plot(x=wavelength, y=spec_smooth, pen=myPen2)
                    region[i].setBrush(region_highlight_brush)
                else:
                    region[i].setBrush(region_highlight_brush2)
                vbs[i] = p2[i].vb

                p2[i].scene().sigMouseMoved.connect(functools.partial(mouseMoved, i))

                pfs.addItem(region[i], ignoreBounds=True)

            else:
                self.plots_lines[i].hide()
                self.plots_cutouts[i].hide()
                self.plots_cutouts_sn[i].hide()

        for i in range(len(self.plots_lines)):
            if z_pix[i] > border and z_pix[i] < len(wavelength)-border:
                try:
                    p2[i_line].scene().sigMouseMoved.connect(functools.partial(mouseMoved, i))
                except:
                    # happens when the found line is exactly at the edge of
                    # the spectral range (e.g. at 0)
                    pass

        #####################################################################

    def center(self):

        qr = self.frameGeometry()
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


def main():
    app = QtWidgets.QApplication(sys.argv)
    main = main_Window()
    main.show()
    app.exec()


if __name__ == '__main__':
    main()

### make list with short names for identifications

short_names = []
for a in lines_ident:
    try:
        short_names.append(poss_ident_short[a])
    except KeyError:
        short_names.append('Rev')

### Now write the identifications in a new fits file!

write_output(catalog, lines_ident, lines_comment, lines_redshifts,
             lines_quality, lines_confidence, poss_ident_short, lines_assoc,
             new_catalog)

print('Well done! :)')
