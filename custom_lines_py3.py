# File containing the lines for classification as well the wavelengths.
# It also has the possible options for identification in the drop down menu
# as well as the short versions (for making it easier to quickly check the
# output catalog).


### Possible lines
# You can adjust the wavelength to your liking or add new lines
# The following are all possible lines that can be matched to found lines, 
# but they are not automatically featured in the drop down menu for manual 
# classification (that would be the next list). 
# Wavelength should be given in Angstrom and vacuum.

# in vacuum, most are taken from (not above 2000 Angstrom)
# https://physics.nist.gov/PhysRefData/ASD/lines_form.html
# some are from http://classic.sdss.org/dr4/algorithms/linestable.html

lbda = {
    'OVI 1': 1031.93,
    'OVI 2': 1037.62,
    'Lya': 1215.67,
    'NV': 1240.81,
    'SiII* 3': 1264.73,
    'OI': 1305.53,
    'SiII* 2': 1309.27,
    'CII* 2': 1335.71,
    'SiIV 1': 1393.76,
    'SiIV 2': 1402.77,
    'SiII* 1': 1533.45,
    'CIV 1': 1548.195, #'Morton1991tab5'
    'CIV 2': 1550.770, #'Morton1991tab5'
    'HeII': 1640.42,
    'OIII 5': 1660.81,
    'OIII 4': 1666.15,
    'SiIII 1': 1882.47,
    'SiIII 2': 1892.03,
    'CIII 1': 1906.683,
    'CIII 2': 1908.734,
    'CII': 2324.8,
    'FeII A1': 2344.21,
    'FeII* 4': 2365.55,
    'FeII A2': 2374.46,
    'FeII A3': 2382.76,
    'FeII* 1': 2396.36,
    'OII 2': 2471.0,
    'FeII A4': 2586.65,
    'FeII A5': 2600.17,
    'FeII* 2': 2612.65,
    'FeII* 3': 2626.45,
    'MgII 1': 2796.35,
    'MgII 2': 2803.53,
    'HeI 3': 3188.37,
    'Ne V': 3426.85,
    'OII 1': 3727.092,
    'OII': 3729.875,
    'NeIII 2': 3869.81,
    'HeI': 3888.65,
    'H8': 3890.15,
    'CaII A1': 3934.777,
    'NeIII 1': 3968.53,
    'CaII A2': 3969.588,
    'He': 3970.08,
    'Hd': 4102.89,
    'Hg': 4341.68,
    'OIII 3': 4363.21,
    'Hb': 4862.68,
    'OIII 1': 4960.295,
    'OIII 2': 5008.240,
    'HeI 2': 5875.68,
    'OI 1': 6302.04,
    'OI 2': 6365.54,
    'NII 1': 6549.86,
    'Ha': 6564.61,
    'NII 2': 6585.27,
    'SII 1': 6718.29,
    'SII 2': 6732.67,
    'ArIII 1': 7136.30
}

### Possible selections
# These are the selections the user can make from the drop-down menu.
# If you want other options, make sure to also define a short version in the
# list below.
# If you also want other lines, make sure they are also in the line list above.
possible_identifications = ["-",
                            "Lya",
                            "OII",
                            "OIII 2",
                            "Ha",
                            "CIII 1",
                            "MgII 1",
                            "Something else",
                            "Continuum residual",
                            "Edge",
                            "Crap",
                            "Revisit", 
                            "OIII 1",
                            "Hb"]

### Short versions
# Define short versions for all your lines and possible identifications

poss_ident_short = {'-':"-", # if a line was forgotten
                    'Something else':"Some",
                    'Continuum residual':"Cres",
                    'Edge':"Edge",
                    'Crap':"Crap",
                    'Revisit':"Rev", 

                    'OVI 1':'O6_1',
                    'OVI 2':'O6_2',
                    'Lya':'Lya',
                    'NV':'N5',
                    'SiII* 3':'Si2*_3',
                    'OI':'O1',
                    'SiII* 2':'Si2*_2',
                    'CII* 2':'C2*_2',
                    'SiIV 1':'Si4_1',
                    'SiIV 2':'Si4_2',
                    'SiII* 1':'Si2*_1',
                    'CIV 1': 'C4_1',
                    'CIV 2': 'C4_2',
                    'HeII': 'He2',
                    'OIII 5':'O3_5',
                    'OIII 4':'O3_4',
                    'SiIII 1':'Si3_1',
                    'SiIII 2':'Si3_2',
                    'CIII 1':'C3_1',
                    'CIII 2':'C3_2',
                    'CII':'C2',
                    'FeII A1': 'Fe2_A1',
                    'FeII* 4': 'Fe2*_4',
                    'FeII A2': 'Fe2_A2',
                    'FeII A3': 'Fe2_A3',
                    'FeII* 1': 'Fe2*_1',
                    'OII 2': 'O2_2',
                    'FeII A4': 'Fe2_A4',
                    'FeII A5': 'Fe2_A5',
                    'FeII* 2': 'Fe2*_2',
                    'FeII* 3': 'Fe2*_3',
                    'MgII 1':'Mg2_1',
                    'MgII 2':'Mg2_2',
                    'HeI 3': 'He1_3',
                    'OII 1': 'O2_1',
                    'Ne V': 'Ne5',
                    'OII':'O2',
                    'NeIII 2': 'Ne3_2',
                    'HeI':'He1',
                    'H8':'H8',
                    'CaII A1': 'CA2_A1',
                    'NeIII 1': 'Ne3_1', 
                    'CaII A2': 'CA2_A2',
                    'He':'He',
                    'Hd':'Hd',
                    'Hg':'Hg',
                    'OIII 3':'O3_3',
                    'Hb': 'Hb',
                    'OIII 1': 'O3_1',
                    'OIII 2': 'O3_2',
                    'HeI 2':'He1_2',
                    'OI 1':'O1_1',
                    'OI 2':'O1_2',
                    'NII 1': 'N2_1',
                    'Ha':'Ha',
                    'NII 2': 'N2_2',
                    'SII 1':'S2_1',
                    'SII 2':'S2_2',
                    'ArIII 1': 'Ar3_1'

}


################################################################################
################### THIS IS TO CHECK IF EVERYTHING IS OK #######################
######################## (don't edit after here) ###############################

def check_dicts(lbda,possible_identifications,poss_ident_short):
    
    for a in list(lbda.keys()):
        try:
            poss_ident_short[a]
        except:
            print("\nERROR: The parameter ",a," does not have a short version yet! Please go to 'custom_lines.py' and append a short version for ",a," in the dictionary 'poss_ident_short' by writing 'parameter':'short'.")
    
    for a in possible_identifications:
        try:
            poss_ident_short[a]
        except:
            print("\nERROR: The parameter ",a," does not have a short version yet! Please go to 'custom_lines.py' and append a short version for ",a," in the dictionary 'poss_ident_short' by writing 'parameter':'short'.")

    for a in poss_ident_short:
        if a not in possible_identifications:
            try:
                lbda[a]
            except:
                print("\nERROR: The parameter ",a," does not have a wavelength yet. Please to to 'custom_lines.py' and append a wavelength in the dictionary 'lbda' by writing 'parameter':wavelength.")
