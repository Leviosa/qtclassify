# get colors for the plots

import matplotlib.pyplot as plt
import numpy as np
import random

# make viridis available
import colormaps as cmaps
''' ### Changed July 2022
plt.register_cmap(name='viridis', cmap=cmaps.viridis)
plt.register_cmap(name='plasma', cmap=cmaps.plasma)
plt.register_cmap(name='inferno', cmap=cmaps.inferno)
plt.register_cmap(name='magma', cmap=cmaps.magma)
'''

# you can get the rgb value of a colormap -> rgb values and transparency

num_colours = 5 # how many colours do you want 

viridis_green = {}
for i in range(num_colours):
    viridis_green[i] = cmaps.viridis(70+i*37)[:3]
viridis_purple = {}
for i in range(num_colours):
    viridis_purple[i] = cmaps.viridis(0+i*40)[:3]
viridis_all = {}
for i in range(num_colours):
    viridis_all[i] = cmaps.viridis(0+i*64)[:3]
viridis_more = {}
for i in range(10):
    viridis_more[i] = cmaps.viridis(0+i*(256/10))[:3]

plasma_pink = {}
for i in range(num_colours):
    plasma_pink[i] = cmaps.plasma(50+i*30)[:3]
plasma_orange = {}
for i in range(num_colours):
    plasma_orange[i] = cmaps.plasma(140+i*20)[:3]
plasma_all = {}
for i in range(num_colours):
    plasma_all[i] = cmaps.plasma(0+i*64)[:3]
plasma_more = {}
for i in range(10):
    plasma_more[i] = cmaps.plasma(0+i*(256/10))[:3]

inferno_all = {}
for i in range(num_colours):
    inferno_all[i] = cmaps.inferno(20+i*55)[:3]

magma_pink = {}
for i in range(num_colours):
    magma_pink[i] = cmaps.magma(50+i*30)[:3]
magma_orange = {}
for i in range(num_colours):
    magma_orange[i] = cmaps.magma(140+i*20)[:3]
magma_all = {}
for i in range(num_colours):
    magma_all[i] = cmaps.magma(20+i*55)[:3]
magma_more = {}
for i in range(10):
    magma_more[i] = cmaps.magma(0+i*(256/10))[:3]


def rgb2hex(rgb):
    r,g,b = rgb
    r=int(r*256)
    g=int(g*256)
    b=int(b*256)
    hex = "#{:02x}{:02x}{:02x}".format(r,g,b)
    return hex

# test colour scheme

'''
num = 10 # length of random numbers
rans = {}
for i in xrange(num):
    rans[i] = np.asarray([random.random() for j in xrange(num)])*(i+1)
x = [i for i in xrange(num)]

for i in xrange(num):
    plt.plot(x,rans[i]+i*3,color=magma_more[i],lw=2)
plt.show()
'''
# those have pretty much the same brightness
standard_green = viridis_green[2]
standard_red   = magma_orange[2]
