# File for options you can click on at the top of the GUI.
# Please change with caution! All lines you want to display need to have a
# wavelength in the dedicated line list file (e.g. custom_lines.txt).
# Also the additionally shown lines need to be sorted by ascending wavelength!
# You can only have up to 4 additional lines for each guess (so 5 lines in
# total).


# Those will be clickable guesses for the found line (buttons at the top)
possible_lines_names = ['Lya','OII','OIII 2','Ha','CIII 1','MgII 1']

# Other displayed lines for each guessed line, can be adjusted to needs.
# Should also contain the clickable guess from the list above.
# Attention: These lines have to be sorted by wavelength! 
dict_other_lines = {
    possible_lines_names[0]: ['Lya','CIV 1','HeII','CIII 1'],
    possible_lines_names[1]: ['OII','NeIII 2','Hb','OIII 1','OIII 2'],
    possible_lines_names[2]: ['OII','Hb','OIII 1','OIII 2','Ha'],
    possible_lines_names[3]: ['OII','Hb','OIII 2','Ha'],
    possible_lines_names[4]: ['CIV 1','CIII 1','CII','MgII 1'],
    possible_lines_names[5]: ['CIV 1','CIII 1','MgII 1','OII'],
}


################################################################################
##################### THIS IS TO CHECK IF EVERYTHING IS OK #####################
########################## (don't edit after here) #############################

def check_opts(possible_lines_names,dict_other_lines,lbda):
    
    for a in possible_lines_names:

        if len(dict_other_lines[a])>5:
            print("\nERROR: You want to display more than 5 lines per guess. This is not possible. Please go to 'custom_options.py' and make sure every list in 'dict_other_lines' has less than or equal to 5 entries.\n")

        if a not in list(lbda.keys()):
            print("\nERROR: The possible guess (buttons at the top) ",a," does not have a wavelength. Please go to 'custom_lines.py' and enter a wavelength in the dictionary 'lbda' by writing 'guess':wavelength.")

        for b in dict_other_lines[a]:
            if b not in list(lbda.keys()):
                print("\nERROR: The line ",b," does not have a wavelength. Please go to 'custom_lines.py' and enter a wavelength in the dictionary 'lbda' by writing 'line':wavelength.")
    
